package testCase;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commonUtil.FileUtil;
import pageObjects.HomePage;
import pageObjects.MyAccountPage;
import pageObjects.loginDetails;
import pageObjects.loginPage;

public class TestLogin {
	
	WebDriver driver;
	
	@BeforeTest
	public void setup(){
			
		    String driverKey=FileUtil.loadProperties().getProperty("driverKey");
		    String driverLocation=FileUtil.loadProperties().getProperty("driverLocation");
            		
			System.setProperty(driverKey,driverLocation);
			
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.get(FileUtil.loadProperties().getProperty("baseURL"));
	}

	/*
	 * Test case for Facebook account Login. 
	 */
	
	@Test
	public void testLogin_facebook(){
		
		loginPage login = new loginPage(driver);
		login.click_On_SignUp_btn();
		
		loginDetails details= new loginDetails(driver);
		
		// Switch between frames 
		driver.switchTo().frame(0);
		String title= driver.getTitle();
		System.out.println(title);
		
		// get all window handles
		String parentWindowHandle = driver.getWindowHandle();
		String childWindow = null;
		Set<String> handles = driver.getWindowHandles(); // get all window handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
			childWindow = iterator.next();
		}
		driver.switchTo().window(childWindow);
	
		details.enter_username_facebook(FileUtil.loadProperties().getProperty("facebookUserName"));
		details.enter_password_facebook(FileUtil.loadProperties().getProperty("facebookPassword"));
		details.click_SignIn_Btn();
		details.click_on_ConfirmBtn();
	}
	
	/*
	 * Test case for Login to website and Verify email.
	 */
	
	@Test
	public void testLogin(){
		
		loginPage login = new loginPage(driver);
		login.click_On_SignUp_btn();
		
		//driver.switchTo().frame(0);
		
		
		String parentWindowHandle = driver.getWindowHandle();
		String childWindow = null;
		
		Set<String> handles = driver.getWindowHandles(); // get all window handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
			childWindow = iterator.next();
		}
		driver.switchTo().window(childWindow).close();
		driver.switchTo().window(parentWindowHandle);
		
		loginDetails details= new loginDetails(driver);
		details.enter_email(FileUtil.loadProperties().getProperty("username"));
		details.enter_password(FileUtil.loadProperties().getProperty("password"));
		details.click_siginBtn();
		
		HomePage homepage = new HomePage(driver);
		homepage.click_on_MyAccount();
		
		MyAccountPage accountpage = new MyAccountPage(driver);
		accountpage.VerifyEmail(FileUtil.loadProperties().getProperty("email"));
	}

}
