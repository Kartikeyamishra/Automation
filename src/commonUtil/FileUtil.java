package commonUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

public class FileUtil {
	
	static String configpath= "C:/Users/kartikeya.mishra/workspace/Selenium_Automation/config.properties";
	
	public static String getEmailFromPropertyFile() throws IOException{
		
		FileReader reader=new FileReader(configpath);
		Properties p=new Properties();  
	    p.load(reader);
	    //System.out.println(p.getProperty("email"));
	    return p.getProperty("email");
	}
	
     public static String getreferenceNumberFromPropertyFile() throws IOException{
		
		FileReader reader=new FileReader(configpath);
		Properties p=new Properties();  
	    p.load(reader);
	    return p.getProperty("referenceNumber");
	}
     
     public static String getcompanyIdFromPropertyFile() throws IOException{
 		
 		FileReader reader=new FileReader(configpath);
 		Properties p=new Properties();  
 	    p.load(reader);
 	    return p.getProperty("companyId");
 	}
     
     public static String getcorrelationIDFromPropertyFile() throws IOException{
  		
  		FileReader reader=new FileReader(configpath);
  		Properties p=new Properties();  
  	    p.load(reader);
  	    return p.getProperty("correlation-ID");
  	}
     
     public static String randomNumberGenerator(){
    	 Random rand = new Random();
    	 int number = rand.nextInt(1000000000);
    	 String referenceNumber= String.valueOf(number);
    	   return referenceNumber;
    	 
     }
     
//     public static String xmlFileToStringConversion() throws IOException{
//    	 
//    	 BufferedReader br = new BufferedReader(new FileReader(new File(xmlPath)));
//    	 String line;
//    	 StringBuilder sb = new StringBuilder();
//
//    	 while((line=br.readLine())!= null){
//    	     sb.append(line.trim());
//    	 }
//    	 String xmlString=sb.toString(); 
//    	 return xmlString; 
//    	 
//    	  }
     
     public static Properties loadProperties() {
 		File file = new File(configpath);
 		FileInputStream fileInput = null;
 		Properties props = new Properties();

 		try {
 			fileInput = new FileInputStream(file);
 			props.load(fileInput);
 			fileInput.close();
 		} catch (FileNotFoundException e) {
 			//log.error("config.properties is missing or corrupt : " + e.getMessage());
 		} catch (IOException e) {
 			//log.error("read failed due to: " + e.getMessage());
 		}

 		return props;
     }


}
