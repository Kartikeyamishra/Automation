package pageObjects;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage {
	
	@FindBy(xpath="/html/body/div[3]/div/div/div[1]/div/div/div[2]/div[1]/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/form/div[3]/div/input")
	private WebElement emailtextFeildvalue;
	

	WebDriver driver;
	
	public MyAccountPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void VerifyEmail(String email){
		String emailActual=emailtextFeildvalue.getText();
		assertEquals(emailActual, email, "Verify email in My Account");
		
	}

}
