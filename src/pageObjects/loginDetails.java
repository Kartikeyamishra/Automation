package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.gargoylesoftware.htmlunit.javascript.configuration.WebBrowser;

public class loginDetails {

	@FindBy(id="email")
	private WebElement facebook_email;
	
	@FindBy(id="pass")
	private WebElement facebook_password;
	
	@FindBy(name="login")
	private WebElement facebook_signInBtn;
	
//	@FindBy(css=".facebook-login-button")
//	private WebElement facebookBtn;
	
	@FindBy(name="__CONFIRM__")
	private WebElement confirmBtn;
	
	@FindBy(name="email")
	private WebElement email_text_field;
	
	@FindBy(name="password")
	private WebElement password_text_field;
	
	@FindBy(css=".offset-xs-1 > button:nth-child(1)")
	private WebElement signInBtn;
	
	WebDriver driver;
	
	public loginDetails(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
//	public void click_On_fb(){
//		facebookBtn.click();
//	}
	
	public void enter_username_facebook(String email){
		facebook_email.sendKeys(email);
	}
	
	public void enter_password_facebook(String password){
		facebook_password.sendKeys(password);
	}
	
	public void click_SignIn_Btn(){
		facebook_signInBtn.click();
	}
	
	public void click_on_ConfirmBtn(){
		confirmBtn.click();
	}

	public void enter_email(String email){
		email_text_field.sendKeys(email);
	}
	
	public void enter_password(String password){
		password_text_field.sendKeys(password);
	}
	
	public HomePage click_siginBtn(){
		signInBtn.click();
		return new HomePage(driver);
	}
	
}
