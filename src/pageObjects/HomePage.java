package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	@FindBy(xpath="/html/body/div[3]/div/div/div[1]/div/div/div[1]/div/div/div[2]/div/a[3]/div")
	private WebElement myAccount;
	
	WebDriver driver;
	
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public MyAccountPage click_on_MyAccount(){
		myAccount.click();
		return new MyAccountPage(driver);
	}

}
