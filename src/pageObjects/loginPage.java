package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class loginPage {
	
	@FindBy(linkText="Sign up")
	private WebElement signUpBtn;
	
	WebDriver driver;
	
	public loginPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public loginDetails click_On_SignUp_btn(){
		signUpBtn.click();
		return new loginDetails(driver);
	}
	

}
